class SearchEntry {
  String policyType;
  String city;
  String speciality;

  SearchEntry(this.policyType, this.city, this.speciality);
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:health_lookup/utils/my_navigator.dart';
import 'package:location/location.dart';
//import 'package:health_lookup/utility/Button.dart';
import 'package:flutter/cupertino.dart';
//import 'package:sliding_up_panel/sliding_up_panel.dart';

class MapView extends StatefulWidget {
  // initialize the email string to show it
  final String name;
  MapView({Key key, @required this.name}) : super(key: key);

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  GoogleMapController mapController;
  Location location = new Location();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<Marker> allMarkers = [];
  final Firestore _database = Firestore.instance;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  @override
  void initState() {
    super.initState();
    allMarkers.add(Marker(
      markerId: MarkerId('myMarker'),
      draggable: false,
      infoWindow: InfoWindow(title: 'Go', snippet: 'what is this?'),
      position: LatLng(24.7136, 46.6753),
    ));
  }

  String dropdownValue = 'Hospital';
  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size;
    return
        // Prevent user from going back step by wrapping with willpopscope
        WillPopScope(
      child: Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListView(
              padding: EdgeInsets.symmetric(vertical: 20),
              children: <Widget>[
                DrawerHeader(
                  child: Text(
                    'Health Lookup',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.greenAccent[700],
                        fontSize: 27,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: RaisedButton(
                        color: Colors.blue[500],
                        textColor: Colors.white,
                        disabledColor: Colors.grey,
                        disabledTextColor: Colors.black,
                        padding: EdgeInsets.all(8.0),
                        splashColor: Colors.blueAccent[500],
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(40.0),
                        ),
                        child: Text('Log Out'),
                        onPressed: () {
                          signOut();
                          MyNavigator.goToLoginPage(context);
                          _scaffoldKey.currentState.openEndDrawer();
                        },
                      ),
                    ),
                    Text(widget.name),
                  ],
                ),
              ],
            ),
          ),
        ),
        appBar: AppBar(
          title: Text('You\'re logged in!'),
        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              myLocationButtonEnabled: false,
              myLocationEnabled: true,
              compassEnabled: true,
              initialCameraPosition: CameraPosition(
                target: LatLng(24.7136, 46.6753),
                zoom: 11.0,
              ),
              markers: Set.from(allMarkers),
            ),
            Positioned(
                right: 10,
                bottom: 30,
                child: FloatingActionButton(
                  heroTag: 2,
                  child: Icon(Icons.filter_list),
                  onPressed: () => _animateToUser(),
                )),
            Positioned(
                right: 10,
                bottom: 100,
                child: FloatingActionButton(
                  heroTag: 1,
                  child: Icon(Icons.my_location),
                  onPressed: () => _animateToUser(),
                )),
          ],
        ),
      ),
      onWillPop: () async => false,
    );
  }

  // create an instance of mapcontroller to manage
  _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  // Changes the mapview to center on the user location
  _animateToUser() async {
    location.onLocationChanged();
    var pos = await location.getLocation();
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(pos.latitude, pos.longitude), zoom: 17.0)));
  }


  // slides the filters drawer Down/up

  //_animateDown() async {}

  static Future<void> signOut() async {
    return FirebaseAuth.instance.signOut();
  }
}

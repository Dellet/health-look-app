// import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MapView2 extends StatefulWidget {
  @override
  State<MapView2> createState() => _MapViewState();
}

class _MapViewState extends State<MapView2> {
  GoogleMapController mapController;
  Location location = new Location();
  final Firestore _database = Firestore.instance;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  createMarkers() {
    // _database.collection('Providers')
    //           .getDocuments().then((docs) {
    //             if(docs.documents.isNotEmpty){
    //               for(int i= 0; i < docs.documents.length; i++) {
    //                 initMarker(docs.documents[i].data, docs.documents[i].documentID);
    //               }
    //             }
    _database.collection('Providers').getDocuments().then((docs) {
      if (docs.documents.isNotEmpty) {
        for (int i = 0; i < docs.documents.length; i++) {
          initMarker(
              docs.documents[i].data['name.en'], docs.documents[i].documentID);
        }
      }
    });
  }

  void initMarker(name, locationId) {
    var markerIdVal = locationId;
    final MarkerId markerId = MarkerId(markerIdVal);

    // creating a new MARKER
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(name['Lat'], name['Lng']),
      infoWindow: InfoWindow(title: name['name.en'], snippet: name['Type.en']),
    );

    setState(() {
      createMarkers();
      super.initState();
      // adding a new marker to map
      markers[markerId] = marker;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          myLocationButtonEnabled: false,
          myLocationEnabled: true,
          compassEnabled: true,
          initialCameraPosition: CameraPosition(
            target: LatLng(24.7136, 46.6753),
            zoom: 11.0,
          ),
          markers: Set<Marker>.of(markers.values),
        ),
        Positioned(
          right: 10,
          bottom: 100,
          child: FloatingActionButton(
            heroTag: 1,
            child: Icon(Icons.my_location),
            onPressed: () => _animateToUser(),
          ),
        ),
      ]),
    );
  }

  // create an instance of mapcontroller to manage
  _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  // Changes the mapview to center on the user location
  _animateToUser() async {
    location.onLocationChanged();
    var pos = await location.getLocation();
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(pos.latitude, pos.longitude), zoom: 17.0)));
  }
}

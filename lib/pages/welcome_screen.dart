import 'package:flutter/material.dart';
import 'package:health_lookup/widgets/custom_flat_button.dart';


final String loginLogo = 'lib/assets/splashlogo.png';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new ListView(
        children: <Widget>[

          Padding(
            padding: EdgeInsets.only(top: 110.0),
            child: Image.asset(
              loginLogo,
              // semanticsLabel: 'Splash logo',
              // color: Colors.white,
              height: 140.0,
              width: 195.0,
              // allowDrawingOutsideViewBox: true,
            ),
                // size: Size(166.0, 232.0),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50.0, right: 15.0, left: 15.0),
            child: Text(
              "Say Hello To Health Look!",
              softWrap: true,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromRGBO(39, 39, 39, 1.0),
                decoration: TextDecoration.none,
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
                fontFamily: "GothamPro",
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 70.0)
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 8.0, horizontal: 60.0),
            child: CustomFlatButton(
              title: "Log In",
              fontSize: 18,
              fontWeight: FontWeight.w500,
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context).pushNamed("/signin");
              },
              splashColor: Colors.blueAccent[600],
              borderColor: Color.fromRGBO(70, 142, 196, 1.0),
              borderWidth: 0,
              color: Color.fromRGBO(70, 142, 196, 1.0),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 8.0, horizontal: 60.0),
            child: CustomFlatButton(
              title: "Sign Up",
              fontSize: 18,
              fontWeight: FontWeight.w500,
              textColor: Color.fromRGBO(70, 142, 196, 1.0),
              onPressed: () {
                Navigator.of(context).pushNamed("/signup");
              },
              splashColor: Colors.blueAccent[600],
              borderColor: Color.fromRGBO(70, 142, 196, 1.0),
              borderWidth: 2,
            ),
          ),
        ],
      ),
    );
  }
}

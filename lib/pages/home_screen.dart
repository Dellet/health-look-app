import 'dart:async';
import 'dart:convert';
// import 'dart:ffi';
import 'package:fluster/fluster.dart';
import 'package:flutter/material.dart';
import 'package:health_lookup/utils/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:health_lookup/utils/user.dart';
import 'package:firebase_database/firebase_database.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:health_lookup/helpers/map_marker.dart';
import 'package:health_lookup/helpers/map_helper.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'package:health_lookup/utils/dropdown_formfield.dart';

class HomeScreen extends StatefulWidget {
  final FirebaseUser firebaseUser;

  HomeScreen({this.firebaseUser});

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    // print(widget.firebaseUser);

    _myActivity = '';
    _myActivityResult = '';

    _clientPlaceType = '';
    _clientPlaceTypeResult = '';

    _clientCity = '';
    _clientCityResult = '';
    // print(markerLocs);
    // _modalTypeTextController = new TextEditingController(text: _type);
  }

  final dbRef = FirebaseDatabase.instance.reference();

  final Completer<GoogleMapController> _mapController = Completer();

  /// Set of displayed markers and cluster markers on the map
  final Set<Marker> _markers = Set();

  /// Minimum zoom at which the markers will cluster
  final int _minClusterZoom = 0;

  /// Maximum zoom at which the markers will cluster
  final int _maxClusterZoom = 19;

  /// [Fluster] instance used to manage the clusters
  Fluster<MapMarker> _clusterManager;

  /// Current map zoom. Initial zoom will be 15, street level
  double _currentZoom = 9;

  /// Map loading flag
  bool _isMapLoading = true;

  /// Markers loading flag
  bool _areMarkersLoading = true;

  /// Url image used on normal markers
  final String _markerImageUrl =
      'https://img.icons8.com/office/80/000000/marker.png';

  /// Url image used on cluster markers
  final String _clusterImageUrl =
      'https://img.icons8.com/officel/80/000000/place-marker.png';

  Color purple = Color(0xff4242ba);
  final primaryColor = const Color(0xFF468EC4);
  final primaryBlackColor = const Color(0xFF272727);

  PanelController _pc = new PanelController();

  final double _initFabHeight = 120.0;
  double _fabHeight;
  double _panelHeightOpen = 500.0;
  double _panelHeightClosed = 95.0;

  // Iterable markerLocs = [];

  TextEditingController _modalTypeTextController;

  /// Called when the Google Map widget is created. Updates the map loading state
  /// and inits the markers.
  ///

  void _onMapCreated(GoogleMapController controller) {
    _mapController.complete(controller);

    setState(() {
      _isMapLoading = false;
    });
    // _snapFromFire();
    _initMarkers();
    // print(allMarkers);
    // tutu();
  }

  // double parseDouble(dynamic value) {
  //   try {
  //     if (value is String) {
  //       return double.parse(value);
  //     } else if (value is double) {
  //       return value;
  //     } else {
  //       return 0.0;
  //     }
  //   } catch (e) {
  //     // return null if double.parse fails
  //     return null;
  //   }
  // }

  final List<LatLng> latLngMarker = [];

  /// Inits [Fluster] and all the markers with network images and updates the loading state.
  void _initMarkers() async {
    // _snapFromFire();
    // var dbProviders = dbRef.child('providers');

    // print(dbRef.child("providers").once());

    final List<MapMarker> markers = [];
    // print(latLngMarker);

    // getCoches(0);

    final db = FirebaseDatabase.instance.reference().child("providers");

    db.once().then((DataSnapshot snapshot) {
      final List<LatLng> latLngMarker = [];

      List<dynamic> values = snapshot.value;
      for (var element in values) {
        if (element != null) {
          print(element["Lat"] + " <------> " + element["Lat"]);
          // print("hreeeeeeeeeeeeeeeeeeeeeeeeeeeee---------------------------->" + element.runtimeType);
          double lati = element["Lat"] is String
              ? double.parse(element["Lat"])
              : element["Lat"];
          double longti = element["Lng"] is String
              ? double.parse(element["Lng"])
              : element["Lng"];
          // double lati = double.parse(element["Lat"].toDouble());
          // double longti = double.parse(element["Lng"].toDouble());
          // final List<LatLng> latLngMarker = [(lati, longti)];
          latLngMarker.add(LatLng(lati, longti));
        }
      }
    });

    for (LatLng markerLocation in latLngMarker) {
      final BitmapDescriptor markerImage =
          await MapHelper.getMarkerImageFromUrl(_markerImageUrl);

      print(latLngMarker);
      markers.add(
        MapMarker(
          id: latLngMarker.indexOf(markerLocation).toString(),
          position: markerLocation,
          icon: markerImage,
        ),
      );
      // print(allMarkers);

    }

    _clusterManager = await MapHelper.initClusterManager(
      markers,
      _minClusterZoom,
      _maxClusterZoom,
      _clusterImageUrl,
    );

    _updateMarkers();
  }

  /// Gets the markers and clusters to be displayed on the map for the current zoom level and
  /// updates state.
  void _updateMarkers([double updatedZoom]) {
    if (_clusterManager == null || updatedZoom == _currentZoom) return;

    if (updatedZoom != null) {
      _currentZoom = updatedZoom;
    }

    setState(() {
      _areMarkersLoading = true;
    });

    _markers
      ..clear()
      ..addAll(MapHelper.getClusterMarkers(_clusterManager, _currentZoom));

    setState(() {
      _areMarkersLoading = false;
    });
  }

  var _value = "1";
  DropdownButton _policyDropDownButton() => DropdownButton<String>(
        items: [
          DropdownMenuItem<String>(
            value: "1",
            child: Text(
              "First",
            ),
          ),
          DropdownMenuItem<String>(
            value: "2",
            child: Text(
              "Second",
            ),
          ),
        ],
        onChanged: (value) {
          setState(() {
            _value = value;
          });
        },
        hint: Text(
          "Please select the number!",
          style: TextStyle(
            color: Colors.blue,
          ),
        ),
        // value: _value,
        elevation: 3,
        style: TextStyle(color: Colors.redAccent, fontSize: 17),
        isDense: true,
        iconSize: 20.0,
      );

  @override
  Widget build(BuildContext context) {
    BorderRadiusGeometry radius = BorderRadius.only(
      topLeft: Radius.circular(24.0),
      topRight: Radius.circular(24.0),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Health Look', style: TextStyle(color: Colors.white)),
        iconTheme: new IconThemeData(color: Colors.white),
        backgroundColor: primaryColor,
      ),
      drawer: Drawer(
        elevation: 1.5,
        child: Column(
          children: <Widget>[
            Expanded(
              // ListView contains a group of widgets that scroll inside the drawer
              child: ListView(
                children: <Widget>[
                  // new Container(child: new DrawerHeader(child: new CircleAvatar()),color: Colors.tealAccent,),
                  new Container(
                    color: primaryColor,
                    child: new DrawerHeader(
                      // padding: EdgeInsets.zero,
                      child: Text('Health Look',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                              color: Colors.white)),
                    ),
                  ),
                ],
              ),
            ),

            // This container holds the align
            Container(
                // This align moves the children to the bottom
                child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    // This container holds all the children that will be aligned
                    // on the bottom and should not scroll with the above ListView
                    child: Container(
                        child: Column(
                      children: <Widget>[
                        Divider(),
                        ListTile(
                            leading: Icon(Icons.help),
                            title: Text('Help and Feedback',
                                style: TextStyle(color: primaryBlackColor))),
                        ListTile(
                          leading: new Icon(Icons.exit_to_app),
                          title: Text('Log Out',
                              style: TextStyle(color: primaryBlackColor)),
                          onTap: () {
                            _logOut();
                            _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      ],
                    )))),
          ],
        ),
      ),
      body: Stack(
        children: <Widget>[
          // Google Map widget
          Opacity(
            opacity: _isMapLoading ? 0 : 1,
            child: GoogleMap(
              mapToolbarEnabled: false,
              initialCameraPosition: CameraPosition(
                target: LatLng(24.476970, 39.637356),
                zoom: _currentZoom,
              ),
              // markers: _markers,
              markers: _markers,
              // markers: Set.from(markerLocs),

              onMapCreated: (controller) => _onMapCreated(controller),
              onCameraMove: (position) => _updateMarkers(position.zoom),
            ),
          ),

          // Map loading indicator
          Opacity(
            opacity: _isMapLoading ? 1 : 0,
            child: Center(child: CircularProgressIndicator()),
          ),
          // Center(child: Text("the Widget behind the sliding panel"),),
          // Map markers loading indicator
          if (_areMarkersLoading)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topCenter,
                child: Card(
                  elevation: 2.0,
                  color: Colors.grey.withOpacity(0.9),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      'Loading',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),

          SlidingUpPanel(
            controller: _pc,
            backdropEnabled: true,
            panel: _panel(),
            body: _body(),
            minHeight: 0,
            maxHeight: 340,
            color: Colors.transparent,
            boxShadow: null,
            // backdropOpacity: 0.0,
            // margin: const EdgeInsets.fromLTRB(24.0, 0.0, 24.0, 0.0),

            // collapsed: Container(

            //   decoration: BoxDecoration(
            //     color: primaryColor,
            //     borderRadius: radius

            //   ),

            //   child: Center(

            //     child: Text(
            //       "Search",
            //       style: TextStyle(color: Colors.white),
            //     ),
            //   ),
            // ),
            borderRadius: radius,
          ),
        ],
      ),
    );
  }

  Widget _body() {
    return Container(
      // alignment: Alignment.bottomRight,
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.end,

        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.symmetric(vertical: 250.0),
          ),
          new FloatingActionButton(
            onPressed: () => _pc.open(),
            child: new Icon(
              Icons.filter_list,
              color: Colors.white,
              size: 24.0,
            ),
            backgroundColor: primaryColor,
          ),
        ],
      ),
    );
  }

  String _myActivity;
  String _myActivityResult;

  String _clientPlaceType;
  String _clientPlaceTypeResult;

  String _clientCity;
  String _clientCityResult;

  String _clientWantedSpeciality;
  String _clientWantedSpecialityResult;

  final formKey = new GlobalKey<FormState>();

  // @override
  // void initState() {
  //   super.initState();
  //   _myActivity = '';
  //   _myActivityResult = '';
  // }

  _saveForm() {
    var form = formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _myActivityResult = _myActivity;
      });
    }
  }

  Widget _panel() {
    String _type;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),

        // color: Color(0xff232f34),
        // color: Colors.green,
        // color: Colors.white,
      ),
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            height: 40,
          ),
          SizedBox(
            height: 300,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(7.5),
                  topRight: Radius.circular(7.5),
                ),
              ),
              child: Stack(
                alignment: Alignment(0, 0),
                overflow: Overflow.visible,
                children: <Widget>[
                  Positioned(
                    // bottom: 2.0,
                    child: ListView(
                      padding: const EdgeInsets.only(
                          top: 50.0, left: 30.0, right: 30.0),
                      // SliverPadding: ,
                      // physics: NeverScrollableScrollPhysics(),
                      children: <Widget>[
                        new Container(
                            // decoration: new BoxDecoration(
                            //   boxShadow: [
                            //     BoxShadow(
                            //       color: Color.fromRGBO(0, 0, 0, 0.05),
                            //       blurRadius: 20.0, // has the effect of softening the shadow
                            //       spreadRadius: 5.0, // has the effect of extending the shadow
                            //       offset: Offset(
                            //         0.0, // horizontal, move right 10
                            //         3.0, // vertical, move down 10
                            //       ),
                            //     )
                            //   ],
                            //   // borderRadius: new BorderRadius.all(Radius.circular(20.0)),
                            //   // gradient: new LinearGradient(...),
                            // ),
                            child: DropDownFormField(
                          titleText: 'Medical type',
                          hintText: 'Please choose type',
                          value: _clientPlaceType,
                          onSaved: (value) {
                            setState(() {
                              _clientPlaceType = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              _clientPlaceType = value;
                            });
                          },
                          dataSource: [
                            {
                              "display": "Hospital",
                              "value": "Hospital",
                            },
                            {
                              "display": "Medical Cetner",
                              "value": "Medical Cetner",
                            },
                            {
                              "display": "Pharmacy",
                              "value": "Pharmacy",
                            },
                          ],
                          textField: 'display',
                          valueField: 'value',
                        )),
                        SizedBox(height: 15.0),
                        Container(
                            // decoration: new BoxDecoration(
                            //   boxShadow: [
                            //     BoxShadow(
                            //       color: Color.fromRGBO(0, 0, 0, 0.05),
                            //       blurRadius: 50.0, // has the effect of softening the shadow
                            //       spreadRadius: 5.0, // has the effect of extending the shadow
                            //     )
                            //   ],
                            //   // borderRadius: new BorderRadius.all(Radius.circular(20.0)),
                            //   // gradient: new LinearGradient(...),
                            // ),
                            // padding: EdgeInsets.symmetric(horizontal: 10),
                            child: DropDownFormField(
                          titleText: 'Сity',
                          hintText: 'Please choose your City',
                          value: _clientCity,
                          onSaved: (value) {
                            setState(() {
                              _clientCity = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              _clientCity = value;
                            });
                          },
                          dataSource: [
                            {
                              "display": "Riyadh",
                              "value": "Riyadh",
                            },
                            {
                              "display": "Jeddah",
                              "value": "Jeddah",
                            },
                            {
                              "display": "Medina",
                              "value": "Medina",
                            },
                          ],
                          textField: 'display',
                          valueField: 'value',
                        )),
                        SizedBox(height: 15.0),
                        Container(
                            // decoration: new BoxDecoration(
                            //   boxShadow: [
                            //     BoxShadow(
                            //       color: Color.fromRGBO(0, 0, 0, 0.05),
                            //       blurRadius: 50.0, // has the effect of softening the shadow
                            //       spreadRadius: 5.0, // has the effect of extending the shadow
                            //     )
                            //   ],
                            //   // borderRadius: new BorderRadius.all(Radius.circular(20.0)),
                            //   // gradient: new LinearGradient(...),
                            // ),
                            // padding: EdgeInsets.symmetric(horizontal: 10),
                            child: DropDownFormField(
                          titleText: 'Speciality',
                          hintText: 'Please choose Speciality',
                          value: _clientWantedSpeciality,
                          onSaved: (value) {
                            setState(() {
                              _clientWantedSpeciality = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              _clientWantedSpeciality = value;
                            });
                          },
                          dataSource: [
                            {
                              "display": "1",
                              "value": "1",
                            },
                            {
                              "display": "2",
                              "value": "2",
                            },
                            {
                              "display": "3",
                              "value": "3",
                            },
                          ],
                          textField: 'display',
                          valueField: 'value',
                        )),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -32,
                    child: Container(
                      // decoration: BoxDecoration(
                      //     borderRadius:
                      //         BorderRadius.all(Radius.circular(60)),
                      //     border: Border.all(
                      //         color: primaryColor, width: 10)),
                      child: Center(
                        child: new RawMaterialButton(
                          onPressed: () {},
                          child: new Text(
                            "GO",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),

                            // Icons.pause,
                            // color: Colors.blue,
                            // size: 30.0,
                          ),
                          shape: new CircleBorder(),
                          elevation: 3.0,
                          fillColor: primaryColor,
                          padding: const EdgeInsets.all(20),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container applyButton(String text) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 7.5, horizontal: 30.0),
      decoration: BoxDecoration(
          color: primaryColor,
          // border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(5.0)),
      child: Text(
        text,
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
    );
  }

  void _logOut() async {
    Auth.signOut();
  }
}

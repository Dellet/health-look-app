import 'package:flutter/material.dart';
import "package:flutter_swiper/flutter_swiper.dart";
import "package:health_lookup/widgets/walkthrough.dart";
import 'package:shared_preferences/shared_preferences.dart';
import 'package:health_lookup/widgets/custom_flat_button.dart';

final String loginLogo = 'lib/assets/splashlogo.png';

class WalkthroughScreen extends StatefulWidget {
  final SharedPreferences prefs;
  final List<Walkthrough> pages = [
  Walkthrough(
      // icon: Icons.info
      icon: Icons.home,
      title: "Information",
      description:
          "Enter your information to find\n healthcare providers based on your health\n insurance policy.",
    ),
  Walkthrough(
    icon: Icons.featured_play_list,
    title: "Filter",
    description: "Filter by City, Type, and Speciality available\n at the healthcare provider.",
  ),
  Walkthrough(
    icon: Icons.room,
    title: "Navigate",
    description:
        "You can click on any of the results\n shown in the map to view more details of it,\n and navigate to it.",
  ),
  ];

  WalkthroughScreen({this.prefs});

  @override
  _WalkthroughScreenState createState() => _WalkthroughScreenState();
}

class _WalkthroughScreenState extends State<WalkthroughScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Swiper.children(
        autoplay: false,
        index: 0,
        loop: false,
        pagination: new SwiperPagination(
          margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 40.0),
          builder: new DotSwiperPaginationBuilder(
              color: Colors.white30,
              activeColor: Colors.white,
              size: 6.5,
              activeSize: 8.0),
        ),
        control: SwiperControl(
          iconPrevious: null,
          iconNext: null,
        ),
        children: _getPages(context),
      ),
    );
  }

  List<Widget> _getPages(BuildContext context) {
    List<Widget> widgets = [];
    for (int i = 0; i < widget.pages.length; i++) {
      Walkthrough page = widget.pages[i];
      widgets.add(
        new Container(
          color: Color.fromRGBO(70, 142, 196, 1.0),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: Icon(
                  page.icon,
                  size: 120.0,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 50.0, right: 15.0, left: 15.0),
                child: Text(
                  page.title,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    decoration: TextDecoration.none,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                    fontFamily: "GothamPro",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  page.description,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    decoration: TextDecoration.none,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w300,
                    fontFamily: "OpenSans",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: page.extraWidget,
              )
            ],
          ),
        ),
      );
    }
    widgets.add(
      new Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // Icon(
              //   Icons.code,
              //   size: 125.0,
              //   color: Colors.white,
              // ),
              new Image.asset(
              loginLogo,
              // semanticsLabel: 'Splash logo',
              // color: Colors.white,
              height: 165.0,
              width: 230.0),
              Padding(
                padding:
                    const EdgeInsets.only(top: 70.0, right: 15.0, left: 15.0),
                child: Text(
                  "Let's find the best provider",
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromRGBO(39, 39, 39, 1.0),
                    decoration: TextDecoration.none,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                    fontFamily: "GothamPro",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                          vertical: 70.0, horizontal: 40.0),
                child: CustomFlatButton(
                  title: "Get Started",
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  textColor: Colors.white,
                  onPressed: () {
                    widget.prefs.setBool('seen', true);
                    Navigator.of(context).pushNamed("/root");
                  },
                  splashColor: Colors.black12,
                  borderColor: Color.fromRGBO(70, 142, 196, 1.0),
                  borderWidth: 2,
                  color: Color.fromRGBO(70, 142, 196, 1.0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    return widgets;
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:health_lookup/utils/my_navigator.dart';

class LangSwitcher extends StatefulWidget {
  @override
  _LangSwitcherState createState() => _LangSwitcherState();
}

class _LangSwitcherState extends State<LangSwitcher> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: SingleChildScrollView(
                  child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('splashlogo.png'),
              Container(
                child: FlatButton(
                  child: Text(
                              'Continue'), 
                  onPressed: () => MyNavigator.goToLoginPage(context)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

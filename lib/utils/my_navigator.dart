import 'package:flutter/material.dart';

class MyNavigator {

  static void goToLoginPage(BuildContext context) {
    Navigator.pushNamed(context, '/lib/pages/loginPage');
  }

  static void goToSearch(BuildContext context) {
    Navigator.pushNamed(context, '/search_screen.dart');
  }

  static void goToMapScreen(BuildContext context) {
    Navigator.popAndPushNamed(context, '/mapview.dart',);
  }

  static void goToSignUpPage(BuildContext context) {
    Navigator.pushNamed(context, '/lib/pages/signUpPage');
  }

}

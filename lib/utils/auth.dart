import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:health_lookup/utils/user.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

enum authProblems { UserNotFound, PasswordNotValid, NetworkError, UnknownError }

class Auth {
  static Future<String> signIn(String email, String password) async {
    // final FirebaseUser user = (await signInWithCredential(credential)).user;
    FirebaseUser user = (await _auth.signInWithEmailAndPassword(email: email, password: password)).user;
    return user.uid;
  }
  
  // static Future<String> signInWithFB(String accessToken) async {
  //     final facebookLogin = FacebookLogin();
  //     final facebookLoginResult = await facebookLogin.logInWithReadPermissions(['email', 'public_profile']);

  //     FacebookAccessToken myToken = facebookLoginResult.accessToken;
      
  //     ///assuming sucess in FacebookLoginStatus.loggedIn
  //     /// we use FacebookAuthProvider class to get a credential from accessToken
  //     /// this will return an AuthCredential object that we will use to auth in firebase
  //     AuthCredential credential = FacebookAuthProvider.getCredential(accessToken: myToken.token);
  
  //     // this line do auth in firebase with your facebook credential.
  //     FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
  //     return user.uid;
      
  //   }

  static Future<String> signUp(String email, String password) async {
    FirebaseUser user = (await _auth.createUserWithEmailAndPassword(email: email, password: password)).user;
    return user.uid;
  }

  static Future<void> signOut() async {
    return FirebaseAuth.instance.signOut();
  }

  static Future<FirebaseUser> getCurrentFirebaseUser() async {
    FirebaseUser user = await _auth.currentUser();
    return user;
  }

  static void addUser(User user) async {
    checkUserExist(user.userID).then((value) {
      if (!value) {
        print("user ${user.firstName} ${user.email} ${user.policyNumber} added");
        Firestore.instance
            .document("users/${user.userID}")
            .setData(user.toJson());
      } else {
        print("user ${user.firstName} ${user.email} ${user.policyNumber} exists");
      }
    });
  }

  static Future<bool> checkUserExist(String userID) async {
    bool exists = false;
    try {
      await Firestore.instance.document("users/$userID").get().then((doc) {
        if (doc.exists)
          exists = true;
        else
          exists = false;
      });
      return exists;
    } catch (e) {
      return false;
    }
  }

  static Stream<User> getUser(String userID) {
    return Firestore.instance
        .collection("users")
        .where("userID", isEqualTo: userID)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return User.fromDocument(doc);
      }).first;
    });
  }

  static String getExceptionText(Exception e) {
    if (e is PlatformException) {
      switch (e.message) {
        case 'There is no user record corresponding to this identifier. The user may have been deleted.':
          return 'User with this e-mail not found.';
          break;
        case 'The password is invalid or the user does not have a password.':
          return 'Invalid password.';
          break;
        case 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.':
          return 'No internet connection.';
          break;
        case 'The email address is already in use by another account.':
          return 'Email address is already taken.';
          break;
        default:
          return 'Unknown error occured.';
      }
    } else {
      return 'Unknown error occured.';
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';


class RoundedButton extends StatelessWidget {
RoundedButton(
      {this.text,
      this.onPressed});

final String text;
final Function onPressed;
  @override
  Widget build(BuildContext context) {
    return Padding(
                padding: EdgeInsets.all(20.0),
                child: SizedBox(
                  width: 220.0,
                  height: 63.0,
                  child: RaisedButton(
                    color: Colors.blue[500],
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent[500],
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(40.0),
                    ),

                    // Send the user to the login page when pressing the button
                    onPressed: onPressed,
                    child: Text(
                      text,
                      style: TextStyle(
                          fontSize: 23.0, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              );
  }

}
